$(document).ready(function(){

    if (location.protocol == 'https:') {
      socketProtocol = 'wss:'
    } else {
      socketProtocol = "ws:"
    }

    var socket = new WebSocket(socketProtocol + '//' + window.location.host + '/ws/home/');

    socket.onopen = function open() {
      console.log('WebSockets connection created.');
    };

    if (socket.readyState == WebSocket.OPEN) {
      socket.onopen();
    }

    socket.onmessage = function(event) {
      data = JSON.parse(event.data)
      $('#memory-usage').html("Memory Usage: " + data.memory_usage + " %");
      $('#cpu-usage').html("CPU Usage: " + data.cpu_usage + " %");
    };
})
