    $(window).on('load', function () {
    var productName = window.location.pathname.split("/")[1];

    $.ajax({
        type: "GET",
        url: "/" + productName + "/operation-history/30",
        success: function(result) {
            let chartData = [];
            $.each( result.db_operation_history, function( key, value ) {
                let data = {
                    type: "line",
                    name: key,
                    showInLegend: true,
                    dataPoints: []
                }
                $.each( value, function( date, amount ) {
                    data.dataPoints.push({
                        x: new Date(date),
                        y: amount
                    })
                });
                chartData.push(data);
            });
            createChart(chartData, "db-operation-history-chart-container", "DB Operation History", "Number of operation");
        },
    });

    $.ajax({
        type: "GET",
        url: "/" + productName + "/run-history/30",
        success: function(result) {
            let chartData = [];
            $.each( result.run_history, function( key, value ) {
                let data = {
                    type: "line",
                    name: key,
                    showInLegend: true,
                    dataPoints: []
                }
                $.each( value, function( date, amount ) {
                    data.dataPoints.push({
                        x: new Date(date),
                        y: amount
                    })
                });
                chartData.push(data);
            });
            createChart(chartData, "run-history-chart-container", "Run History", "Start (1) / Stop (0)");
        },
    });
    });

    function createChart(chartData, chartId, titleText, axisYTitle){
        var chart = new CanvasJS.Chart(chartId, {
            title: {
                text: titleText,
                horizontalAlign: "left"
            },
            axisY:{
                title: axisYTitle,
            },
            legend: {
                cursor: "pointer",
                verticalAlign: "top",
                horizontalAlign: "center",
                dockInsidePlotArea: true,
                itemclick: toogleDataSeries
            },
            data: chartData
        });
        
        chart.render();

        if (chartId == "run-history-chart-container") {
            chart.axisY[0].set("interval", 1);
            chart.axisY[0].set("maximum", 1);
        }

        function toogleDataSeries(e){
            if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                e.dataSeries.visible = false;
            } else{
                e.dataSeries.visible = true;
            }
            chart.render();
        }
    }


