$(document).ready(function() {
    var t = $('#notification-mail-receiver-list-table').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    initNewReceiverClick();
});

function initNewReceiverClick(){
	$('#new-receiver-button').click(function(){
    	$('#receiver-email').val('');
    	$('#modal-add-new-receiver').modal('show');
	});
}