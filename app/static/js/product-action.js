function startProduct(productName){
    $.ajax({
        type: "GET",
        url: "/" + productName + "/start",
        success: function(result) {
            $('#frontend-status-' + productName).html(result.frontend_status);
            $('#backend-status-' + productName).html(result.backend_status);

            if (result.frontend_status == "Running") {
                $('#frontend-status-' + productName).removeClass("text-danger");
                $('#frontend-status-' + productName).addClass("text-success");
            }

            if (result.backend_status === "Running") {
                $('#backend-status-' + productName).removeClass("text-danger");
                $('#backend-status-' + productName).addClass("text-success");
            }

            if (result.frontend_status === "Running" || result.backend_status === "Running") {
                $('#action-when-stop-' + productName).hide();
                $('#action-when-running-' + productName).show();
                current_running_product_count = parseInt($('#running-product-count').html())
                $('#running-product-count').html(current_running_product_count + 1);
            }
        }
    });
}

function stopProduct(productName){
    $.ajax({
        type: "GET",
        url: "/" + productName + "/stop",
        success: function(result) {
            $('#frontend-status-' + productName).html(result.frontend_status);
            $('#backend-status-' + productName).html(result.backend_status);

            if (result.frontend_status == "Stop") {
                $('#frontend-status-' + productName).removeClass("text-success");
                $('#frontend-status-' + productName).addClass("text-danger");
            }

            if (result.backend_status === "Stop") {
                $('#backend-status-' + productName).removeClass("text-success");
                $('#backend-status-' + productName).addClass("text-danger");
            }

            if (result.frontend_status !== "Running" && result.backend_status !== "Running") {
                $('#action-when-stop-' + productName).show();
                $('#action-when-running-' + productName).hide();
                current_running_product_count = parseInt($('#running-product-count').html())
                $('#running-product-count').html(current_running_product_count - 1);
            }
        }
    });
}

function restartProduct(productName){
    $.ajax({
        type: "GET",
        url: "/" + productName + "/restart",
        success: function(result) {
            if (result.frontend_status == "Stop") {
                $('#frontend-status-' + productName).removeClass("text-success");
                $('#frontend-status-' + productName).addClass("text-danger");
            }

            if (result.backend_status === "Stop") {
                $('#backend-status-' + productName).removeClass("text-success");
                $('#backend-status-' + productName).addClass("text-danger");
            }

            if (result.frontend_status !== "Running" && result.backend_status !== "Running") {
                $('#action-when-stop-' + productName).show();
                $('#action-when-running-' + productName).hide();
                current_running_product_count = parseInt($('#running-product-count').html())
                $('#running-product-count').html(current_running_product_count - 1);
            }
        }
    });
}
