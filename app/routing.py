from channels.routing import URLRouter
from django.urls import path

from .consumers import HomeConsumer

websockets = URLRouter([
    path(
        "ws/home/", HomeConsumer,
        name="home",
    ),
])
