from django.urls import path
from .views import index, product_detail, start_product, stop_product, restart_product, \
	operation_history, run_history, notification_mail_receiver, activate_notification_mail, \
	deactivate_notification_mail, add_notification_mail_receiver, delete_notification_mail_receiver

urlpatterns = [
	path('', index),
	path('notification-mail/receiver', notification_mail_receiver),
	path('notification-mail/receiver/add', add_notification_mail_receiver),
	path('notification-mail/receiver/delete', delete_notification_mail_receiver),
	path('notification-mail/activate', activate_notification_mail),
	path('notification-mail/deactivate', deactivate_notification_mail),
	path('<str:product>/detail', product_detail),
	path('<str:product>/operation-history/<int:requested_days>', operation_history),
	path('<str:product>/run-history/<int:requested_days>', run_history),
	path('<str:product>/start', start_product),
	path('<str:product>/stop', stop_product),
	path('<str:product>/restart', restart_product),
]
