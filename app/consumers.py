import json
import os

from channels.consumer import SyncConsumer
from django.conf import settings


class HomeConsumer(SyncConsumer):

    def websocket_connect(self, event):
        self.send({
            "type": "websocket.accept",
        })

        while True:
            self.send({
                'type': 'websocket.send',
                'text': json.dumps(self.memory_and_cpu_usage())
            })

    def websocket_receive(self, event):
        pass

    def websocket_disconnect(self, event):
        pass

    def memory_and_cpu_usage(self):
        total_memory = os.popen("free -m | grep Mem | awk '{print $2}'").read()
        memory_used = os.popen("free -m | grep Mem | awk '{print $3}'").read()
        memory_usage = str(100 * int(memory_used) // int(total_memory))
        cpu_usage = os.popen('bash ' + os.path.join(settings.BASE_DIR, "app", "scripts", "cpu_usage.sh")).read()

        json_data = {
            "memory_usage": memory_usage,
            "cpu_usage": cpu_usage
        }

        return json_data
