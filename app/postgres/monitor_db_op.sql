CREATE TABLE operationhistory(
   id SERIAL PRIMARY KEY,
   timestamp VARCHAR,
   operation VARCHAR
);

CREATE OR REPLACE FUNCTION updateoperationhistory()
RETURNS trigger AS
$$
 BEGIN
  IF (TG_OP = 'INSERT') THEN
   INSERT INTO operationhistory(timestamp, operation)
   VALUES(current_timestamp, 'INSERT');
   RETURN NEW;
  END IF;

  IF (TG_OP = 'UPDATE') THEN
   INSERT INTO operationhistory(timestamp, operation)
   VALUES(current_timestamp, 'UPDATE');
   RETURN NEW;
  END IF;

  IF (TG_OP = 'DELETE') THEN
   INSERT INTO operationhistory(timestamp, operation)
   VALUES(current_timestamp, 'DELETE');
   RETURN OLD;
  END IF;
 END;
$$
LANGUAGE plpgsql;

DO $$
DECLARE
    t text;
BEGIN
    FOR t IN 
      SELECT
        quote_ident(table_name) as tab_name
      FROM
        information_schema.tables
      WHERE
        table_schema NOT IN ('pg_catalog', 'information_schema')
        AND table_schema NOT LIKE 'pg_toast%'
        AND quote_ident(table_name) NOT LIKE 'operationhistory'
    LOOP
        EXECUTE format('CREATE TRIGGER updateoperationhistorytrigger
                        AFTER INSERT OR UPDATE OR DELETE ON %I
                        FOR EACH ROW EXECUTE PROCEDURE updateoperationhistory()',
                        t);
    END LOOP;
END;
$$ LANGUAGE plpgsql;

