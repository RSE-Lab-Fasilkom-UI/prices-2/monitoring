#!/bin/bash
if [ "$#" -ne 1 ]; then
    echo "please provide product name as argument";
    exit 0;
fi

if [ ! -f /var/www/products/$1/logs/nginx_proxy_error.log ]; then
    exit 0;
fi

tail -c 1MB /var/www/products/$1/logs/nginx_proxy_error.log
