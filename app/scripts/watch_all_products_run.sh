#!/bin/bash

products=$(ls /var/www/products/nginx/ | sed 's/.conf//g')

while IFS= read -r product; do
    nohup bash /var/www/monitoring/app/scripts/watch_product_run_backend.sh $product &
    nohup bash /var/www/monitoring/app/scripts/watch_product_run_frontend.sh $product &
done <<< "$products"
