#!/bin/bash

OLD_MEMORY_USAGE=0;

while [ 1 ]
do
    TOTAL_MEMORY="$(free -m | grep Mem | awk '{print $2}')"
    MEMORY_USED="$(free -m | grep Mem | awk '{print $3}')"
    MEMORY_USAGE="$(($MEMORY_USED * 100 / $TOTAL_MEMORY))"

    if [[ $MEMORY_USAGE -ge 90 && $OLD_MEMORY_USAGE -lt 90 ]]
    then
        bash /var/www/monitoring/app/scripts/send_notification_mail.sh
    fi

    OLD_MEMORY_USAGE=$MEMORY_USAGE
    sleep 1
done
