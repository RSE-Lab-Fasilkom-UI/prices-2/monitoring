#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "please provide product name as argument";
    exit 0;
fi

if [ ! -d /var/www/products/$1/frontend ]; then
    echo -n "-";
    exit 0;
fi

if [ $(ls /var/www/products/$1/frontend | grep instance.pid) ]; then
    echo -n "Running";
else
    echo -n "Stop";
fi
