#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "please provide product name as argument";
    exit 0;
fi

if [ ! -f /var/www/products/$1/$1.json ]; then
    echo "-";
    exit 0;
fi

username=$(cat /var/www/products/$1/$1.json |
grep db_username | sed "s/.*db_username\":\s*\"//;s/\".*//")

password=$(cat /var/www/products/$1/$1.json |
grep db_password | sed "s/.*db_password\":\s*\"//;s/\".*//")

PGPASSWORD=$password psql -h 157.245.154.194 -U $username \
-c "SELECT SUM(n_live_tup) FROM pg_stat_user_tables;" |
tail -n+3 | head -n1 | sed "s/\s*//g"
