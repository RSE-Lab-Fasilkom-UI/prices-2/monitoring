#!/bin/bash

products=$(ls /var/www/products/nginx/ | sed 's/.conf//g')

while IFS= read -r product; do
    bash /var/www/monitoring/app/scripts/create_trigger_for_db_operation_history_specific_product.sh $product
done <<< "$products"
