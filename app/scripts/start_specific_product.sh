#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "please provide product name as argument";
    exit 0;
fi
echo "starting "$1" ..."
/var/www/engine/cli/runner.py -cfg /var/www/engine/cli/config.ini start "$1";
