#!/bin/bash

if [ ! -f /var/www/monitoring/SENDGRID_API_KEY ]; then
    echo "Sendgrid api key not found, please provide api key on /var/www/monitoring/SENDGRID_API_KEY";
    exit 0;
fi

SENDGRID_API_KEY="$(cat /var/www/monitoring/SENDGRID_API_KEY)"
FROM_EMAIL="monitoring@splelive.id"
FROM_NAME="Monitoring AISCO SPLELIVE"
SUBJECT="Memory Usage Almost Reach 100%"
BODY_HTML="<h4>Memory Usage Almost Reach 100%</h4><p>Please check the detail on Monitoring AISCO SPLELIVE Website</p><a href='http://maintenanceboard.splelive.xyz/'>Check Detail</a>"

email_to_list="$(sqlite3 /var/www/monitoring/db.sqlite3 'SELECT * FROM app_notificationmailreceiver')"

while IFS= read -r email_to; do

    maildata='{"personalizations": [{"to": [{"email": "'${email_to}'"}]}],"from": {"email": "'${FROM_EMAIL}'",
	    "name": "'${FROM_NAME}'"},"subject": "'${SUBJECT}'","content": [{"type": "text/html", "value": "'${BODY_HTML}'"}]}'

    curl --request POST \
      --url https://api.sendgrid.com/v3/mail/send \
      --header 'Authorization: Bearer '$SENDGRID_API_KEY \
      --header 'Content-Type: application/json' \
      --data "'$maildata'"

done <<< "$email_to_list"

