#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "please provide product name as argument";
    exit 0;
fi

nohup bash /var/www/monitoring/app/scripts/watch_product_run_backend.sh $1 &
nohup bash /var/www/monitoring/app/scripts/watch_product_run_frontend.sh $1 &
