#!/bin/bash

if [ ! -f /var/www/products/$1/admin/properties.json ]; then
    cat /var/www/monitoring/app/json/empty_properties.json;
    exit 0;
fi

cat /var/www/products/$1/admin/properties.json
