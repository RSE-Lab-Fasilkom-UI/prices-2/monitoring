#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "please provide product name as argument";
    exit 0;
fi

if [ ! -d /var/www/products/$1/frontend ]; then
    exit 0;
fi

cd /var/www/products/$1/frontend

inotifywait -e close_write,moved_to,create,delete -m . |
while read -r directory events filename; do
  if [ "$filename" = "instance.pid" ]; then
    if [ -f "instance.pid" ]; then
        echo -n "start," >> /var/www/products/$1/logs/product_run_frontend.log;
        echo $(date +"%Y-%m-%d %T %Z") >> /var/www/products/$1/logs/product_run_frontend.log;
    else
        echo -n "stop," >> /var/www/products/$1/logs/product_run_frontend.log;
        echo $(date +"%Y-%m-%d %T %Z") >> /var/www/products/$1/logs/product_run_frontend.log;
    fi
  fi
done
