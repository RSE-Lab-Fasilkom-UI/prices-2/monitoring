#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "please provide product name as argument";
    exit 0;
fi

if [ ! -f /var/www/products/$1/logs/product_run_frontend.log ]; then
    exit 0;
fi

tac /var/www/products/$1/logs/product_run_frontend.log | uniq
