from django.db import models


class NotificationMailReceiver(models.Model):
    email = models.EmailField(max_length=255, primary_key=True)
