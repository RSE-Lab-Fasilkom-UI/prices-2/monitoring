import json
import os
from datetime import datetime, timedelta

from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.http import JsonResponse
from django.shortcuts import render, redirect

from .models import NotificationMailReceiver

response = {}


def index(request):
    products_name = os.popen(
        'bash ' + os.path.join(settings.BASE_DIR, "app", "scripts", "all_products.sh")).read().splitlines()
    total_memory = os.popen("free -m | grep Mem | awk '{print $2}'").read()
    memory_used = os.popen("free -m | grep Mem | awk '{print $3}'").read()
    cpu_usage = os.popen('bash ' + os.path.join(settings.BASE_DIR, "app", "scripts", "cpu_usage.sh")).read()

    running_product = 0
    products = []
    for name in products_name:
        product = {}
        product['name'] = name

        number_of_db_rows = os.popen('bash '
                                     + os.path.join(settings.BASE_DIR,
                                                    "app",
                                                    "scripts",
                                                    "count_db_row_spesific_product.sh " + name)).read()

        product['number_of_db_rows'] = number_of_db_rows if number_of_db_rows else 0

        product['frontend_status'] = get_frontend_status(name)
        product['backend_status'] = get_backend_status(name)

        if product['frontend_status'] == "Running" or product['backend_status'] == "Running":
            running_product += 1
            product['status'] = "Running"
        elif product['frontend_status'] == "-" and product['backend_status'] == "-":
            product['status'] = "-"
        else:
            product['status'] = "Stop"

        products.append(product)

    response['products'] = products
    response['running_product'] = running_product
    response['amount_of_products'] = len(products)
    response['total_memory'] = total_memory
    response['memory_usage'] = 100 * int(memory_used) // int(total_memory)
    response['cpu_usage'] = cpu_usage

    return render(request, 'index.html', response)


def product_detail(request, product):
    access_log = os.popen('bash '
                          + os.path.join(settings.BASE_DIR,
                                         "app",
                                         "scripts",
                                         "get_access_log_specific_product.sh " + product)).read().splitlines()
    response['access_log'] = access_log

    error_log = os.popen('bash '
                          + os.path.join(settings.BASE_DIR,
                                         "app",
                                         "scripts",
                                         "get_error_log_specific_product.sh " + product)).read().splitlines()
    response['error_log'] = error_log

    properties_json = os.popen('bash '
                               + os.path.join(settings.BASE_DIR,
                                              "app",
                                              "scripts",
                                              "get_properties_specific_product.sh " + product)).read()
    properties = json.loads(properties_json)
    response['properties'] = properties

    return render(request, 'product_detail.html', response)


def start_product(request, product):
    os.system('bash '
              + os.path.join(settings.BASE_DIR,
                             "app",
                             "scripts",
                             "start_specific_product.sh " + product))

    frontend_status = get_frontend_status(product)
    backend_status = get_backend_status(product)

    return JsonResponse({
        "code": "200",
        "frontend_status" : get_frontend_status(product),
        "backend_status" : get_backend_status(product)
    })


def stop_product(request, product):
    os.system('bash '
              + os.path.join(settings.BASE_DIR,
                             "app",
                             "scripts",
                             "stop_specific_product.sh " + product))

    frontend_status = get_frontend_status(product)
    backend_status = get_backend_status(product)

    return JsonResponse({
        "code": "200",
        "frontend_status" : get_frontend_status(product),
        "backend_status" : get_backend_status(product)
    })


def restart_product(request, product):
    os.system('bash '
              + os.path.join(settings.BASE_DIR,
                             "app",
                             "scripts",
                             "restart_specific_product.sh " + product))

    frontend_status = get_frontend_status(product)
    backend_status = get_backend_status(product)

    return JsonResponse({
        "code": "200",
        "frontend_status" : get_frontend_status(product),
        "backend_status" : get_backend_status(product)
    })


def operation_history(request, product, requested_days):
    db_operation_history = os.popen('bash '
                                    + os.path.join(settings.BASE_DIR,
                                                   "app",
                                                   "scripts",
                                                   "get_db_operation_history_specific_product.sh "
                                                   + product)).read().splitlines()

    current_date = datetime.today()

    insert_operation = {}
    update_operation = {}
    delete_operation = {}

    for i in range(requested_days - 1, -1, -1):
        date_of_operation = (current_date - timedelta(days=i)).strftime("%Y-%m-%d")
        insert_operation[date_of_operation] = 0
        update_operation[date_of_operation] = 0
        delete_operation[date_of_operation] = 0

    for row in db_operation_history:
        date_of_operation = row.split()[0]

        date_diff = (current_date - datetime.strptime(date_of_operation, "%Y-%m-%d")).days
        if (date_diff >= requested_days):
            continue

        operation_name = row.split(",")[1]
        if operation_name == "INSERT":
            insert_operation[date_of_operation] += 1
        elif operation_name == "UPDATE":
            update_operation[date_of_operation] += 1
        else:
            delete_operation[date_of_operation] += 1

    return JsonResponse({
        "db_operation_history": {
            "Insert": insert_operation,
            "Update": update_operation,
            "Delete": delete_operation
        }
    })


def run_history(request, product, requested_days):
    run_backend_log = os.popen('bash '
                               + os.path.join(settings.BASE_DIR,
                                              "app",
                                              "scripts",
                                              "get_product_run_log_backend.sh " + product)).read().splitlines()

    run_backend_history = {}

    for history in run_backend_log:
        splitted_history = history.split(",")
        run_status = splitted_history[0]
        run_datetime = splitted_history[1]

        date_diff = (datetime.today() - datetime.strptime(run_datetime, "%Y-%m-%d %H:%M:%S %Z")).days
        if (date_diff >= requested_days):
            break  # log already sorted by time order

        if run_status == "start":
            run_backend_history[run_datetime] = 1
        else:
            run_backend_history[run_datetime] = 0

    run_frontend_log = os.popen('bash '
                                + os.path.join(settings.BASE_DIR,
                                               "app",
                                               "scripts",
                                               "get_product_run_log_frontend.sh " + product)).read().splitlines()

    run_frontend_history = {}

    for history in run_frontend_log:
        splitted_history = history.split(",")
        run_status = splitted_history[0]
        run_datetime = splitted_history[1]

        date_diff = (datetime.today() - datetime.strptime(run_datetime, "%Y-%m-%d %H:%M:%S %Z")).days
        if (date_diff >= requested_days):
            break  # log already sorted by time order

        if run_status == "start":
            run_frontend_history[run_datetime] = 1
        else:
            run_frontend_history[run_datetime] = 0

    return JsonResponse({
        "run_history": {
            "Backend": run_backend_history,
            "Frontend": run_frontend_history
        }
    })


def notification_mail_receiver(request):
    watcher_process = os.popen('ps ax | grep -v grep | grep watch_memory_usage.sh | wc -l') \
        .read()
    if int(watcher_process) > 0:
        notification_mail_status = "Active"
    else:
        notification_mail_status = "Not Active"

    mail_receivers = list(NotificationMailReceiver.objects.values())
    mail_receivers.reverse()

    response['notification_mail_status'] = notification_mail_status
    response['mail_receivers'] = mail_receivers

    return render(request, 'notification_mail_receiver.html', response)


def activate_notification_mail(request):
    os.popen('nohup bash /var/www/monitoring/app/scripts/watch_memory_usage.sh &')

    return redirect("/notification-mail/receiver")


def deactivate_notification_mail(request):
    os.popen('ps ax | grep -v grep | grep watch_memory_usage.sh |'
             + ' awk {"print $1"} | xargs kill -TERM')

    return redirect("/notification-mail/receiver")


def add_notification_mail_receiver(request):

    email = request.POST.get('email', '')

    try:
        validate_email(email)
    except ValidationError as error:
        return render(request, 'notification_mail_receiver.html',
                      {'error': 'Email address is not valid, please try again'})

    NotificationMailReceiver.objects.create(email=email)

    return redirect("/notification-mail/receiver")


def delete_notification_mail_receiver(request):

    email = request.POST.get('email', '')
    NotificationMailReceiver.objects.filter(email=email).delete()

    return redirect("/notification-mail/receiver")

def get_frontend_status(name):
    frontend_status = os.popen('bash '
                                + os.path.join(settings.BASE_DIR,
                                                "app",
                                                "scripts",
                                                "check_running_frontend_spesific_product.sh " + name)).read()
    return frontend_status

def get_backend_status(name):
    backend_status = os.popen('bash '
                                + os.path.join(settings.BASE_DIR,
                                                "app",
                                                "scripts",
                                                "check_running_backend_spesific_product.sh " + name)).read()
    return backend_status

