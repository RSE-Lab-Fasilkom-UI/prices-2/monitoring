# Monitoring AISCO

- Homepage  
  Show list of products with backend status, frontend status, number of rows in product database, and some action that can be executed for a product (start, stop, restart, or show detail).

- Detail Page  
  Show some of product properties, access log, database operation history chart, and product run history chart.

- Notification Mail Receiver Page  
  Show list of notification mail receiver when memory usage exceeds the limit. Provide action to add or delete notification mail receiver, also activate or deactivate the notification mail.

## Scripts (app/scripts)

- all_products.sh  
  Show all aisco products.

- check_running_backend_spesific_product.sh  
  Check whether a product's backend is running or not.
  
  Takes a single argument:  
  $1 - The product

- check_running_frontend_spesific_product.sh  
  Check whether a product's frontend is running or not.

  Takes a single argument:  
  $1 - The product

- count_db_row_spesific_product.sh  
  Count total row of database from a product, the database expected to be postgre sql.

  Takes a single argument:  
  $1 - The product

- count_product.sh  
  Count number of aisco's product.

- cpu_usage.sh  
  Check cpu usage.

- get_access_log_specific_product.sh  
  Get access log from a product, limited to 1 MB.

  Takes a single argument:  
  $1 - The product

- get_db_operation_history_specific_product.sh  
  Get history of database operation in a product (INSERT, UPDATE, and DELETE). The database expected to be postgre sql.

  Takes a single argument:  
  $1 - The product

- get_product_run_log_backend.sh  
  Get a log that contain information when a product's backend start and stop.

  Takes a single argument:  
  $1 - The product

- get_product_run_log_frontend.sh  
  Get a log that contain information when a product's frontend start and stop.

  Takes a single argument:  
  $1 - The product

- get_properties_specific_product.sh  
  Get properties from a product.

  Takes a single argument:  
  $1 - The product

- start_specific_product.sh  
  Start a product.

  Takes a single argument:  
  $1 - The product

- stop_specific_product.sh  
  Stop a product.

  Takes a single argument:  
  $1 - The product


- restart_specific_product.sh  
  Restart a product.

  Takes a single argument:  
  $1 - The product

- send_notification_mail.sh  
  Send a notification mail to every email address listed on this app's sqlite database. The notification mail contain information that memory usage almost reach 100%. Using sendgrid to send mail, the api key should be written on SENDGRID_API_KEY file.

- watch_product_run_backend.sh  
  Watch a product's backend, then write to a log when it is starting or stopping.

  Takes a single argument:  
  $1 - The product

- watch_product_run_frontend.sh  
  Watch a product's frontend, then write to a log when it is starting or stopping.

  Takes a single argument:  
  $1 - The product

- watch_product_run.sh  
  Execute watch_product_run_backend.sh and watch_product_run_frontend.sh for a product.

  Takes a single argument:  
  $1 - The product

- watch_all_products_run.sh  
  Execute watch_product_run.sh for all product.

- watch_memory_usage.sh  
  Watch memory usage, run send_notification_mail.sh when the memory usage greater than or equal to 90%.

- create_trigger_for_db_operation_history_specific_product.sh  
  Run /var/www/monitoring/app/postgres/monitor_db_op.sql on a product database.  
  The sql file will create a table for storing db operation history and a trigger for the table.  
  The database expected to be postgre sql.

  Takes a single argument:  
  $1 - The product  

- create_trigger_for_db_operation_history_all_product.sh  
  Run create_trigger_for_db_operation_history_specific_product.sh for all product.
